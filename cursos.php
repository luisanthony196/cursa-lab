<?php
$indice=1;
include('includes/header.php');
?>
<main>
    <section class="indices">
        <div class="container">
            <?php
            $ruta=array(0,1);
            include('includes/breadcrumb.php');
            ?>
        </div>
    </section>
    <section class="grillas">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/adobe-xd.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/Android Material Design.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/angular-material.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/angular.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/AuditionDesdeCero.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/design-2018.jpg" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/firebase-web.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/git_0.jpg" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/java-interfaces.jpg" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-xl-4 mb-3">
                    <div class="card">
                        <img src="img/linux_0.png" class="card-img-top">
                        <div class="card-body d-flex flex-column p-4">
                            <h5 class="card-title">Aspectos básicos de la cobranza</h5>
                            <p class="card-text">Cursos diseñados para explorar las funcionalidades de Cursalab.</p>
                            <a href="#" class="btn btn-outline-warning align-self-center">Ver más</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include('includes/footer.php'); ?>