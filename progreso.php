<?php
$indice=2;
include('includes/header.php');
?>
<main>
    <section class="indices">
        <div class="container">
            <?php
            $ruta=array(0,2);
            include('includes/breadcrumb.php');
            ?>
        </div>
    </section>
    <section class="cursos">
            <article class="resumen">
                <div class="container mb-5">
                    <h2>Resumen de Cursos</h2>
                    <div class="row">
                        <table class="table text-white">
                            <tbody>
                                <tr>
                                    <td width="33.33%"><span>12</br>TOTAL</span></td>
                                    <td width="33.33%"><span>8</br>APROBADOS<i class="fas fa-check"></i></span></td>
                                    <td width="33.33%"><span>4</br>DESAPROBADOS<i class="fas fa-times"></i></span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
            <article class="colapsado">
                <div class="container mb-5">
                    <h2>Cursos Realizados</h2>
                    <div class="row">
                        <table class="table text-white">
                            <thead>
                                <tr>
                                <th scope="col" width="45%">Curso</th>
                                <th scope="col" width="12%">Nota</th>
                                <th scope="col" width="12%">Intento</th>
                                <th scope="col" width="31%">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Indagación de las necesidades</td>
                                    <td>20.00</td>
                                    <td>02</td>
                                    <td>Aprobado<i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Técnicas de motivación para el aprendizaje</td>
                                    <td>18.00</td>
                                    <td>01</td>
                                    <td>Aprobado<i class="fas fa-check"></i></td>
                                </tr>
                                <tr>
                                    <td>Indagación de las necesidades</td>
                                    <td>04.00</td>
                                    <td>03</td>
                                    <td>Desaprobado<i class="fas fa-times"></i></td>
                                </tr>
                                <tr>
                                    <td>Técnicas de motivación para el aprendizaje</td>
                                    <td>19.00</td>
                                    <td>01</td>
                                    <td>Aprobado<i class="fas fa-check"></i></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
            <article class="colapsado">
                <div class="container mb-3">
                    <h2>Cursos Pendientes</h2>
                    <div class="row">
                        <table class="table text-white">
                            <thead>
                                <tr>
                                <th scope="col" width="45%">Curso</th>
                                <th scope="col" width="30%">Requisitos</th>
                                <th scope="col" width="25%">Estado</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><a class="text-white" data-toggle="collapse" href="#collapse1" role="button" aria-expanded="false" aria-controls="collapse1">Indagación de las necesidades<i class="fas fa-chevron-down"></i></a></td>
                                    <td>Taller de creatividad</td>
                                    <td class="boton"><a class="btn btn-success" href="#" role="button">Ir al curso</a></td>
                                </tr>
                                <tr class="collapse" id="collapse1">
                                    <td colspan="3">
                                        <ul>
                                            <li>Indagación de las necesidades</li>
                                            <li>Técnicas de motivación para el aprendizaje</li>
                                        </ul>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a href="#collapse1" class="text-white">Técnicas de motivación para el aprendizaje<i class="fas fa-chevron-down"></i></a></td>
                                    <td>Taller de creatividad</td>
                                    <td class="boton"><a class="btn btn-success" href="#" role="button">Realizar requisito</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#collapse1" class="text-white">Indagación de las necesidades<i class="fas fa-chevron-down"></i></a></td>
                                    <td>Taller de creatividad</td>
                                    <td class="boton"><a class="btn btn-success" href="#" role="button">Ir al curso</a></td>
                                </tr>
                                <tr>
                                    <td><a class="text-white" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2">Técnicas de motivación para el aprendizaje<i class="fas fa-chevron-down"></i></a></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr class="collapse" id="collapse2">
                                    <td colspan="3">
                                        <ul>
                                            <li>Indagación de las necesidades</li>
                                            <li>Técnicas de motivación para el aprendizaje</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </article>
    </section>
</main>
<?php include('includes/footer.php'); ?>