<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://kit.fontawesome.com/c09cdcc56d.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/main.css">
    <title>Cursa Lab</title>
</head>
<body>
    <header>
        <div class="container-fluid">
            <div class="row justify-content-between mb-3">
                <div class="col-12 col-lg-6">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="logo" style="background-image: url(img/logo.jpg);"></div>
                        <ul class="navbar-nav usuario">
                            <li class="nav-item">
                                <a class="nav-link" href="#"><i class="far fa-user"></i><span>Gabriel Rios</span></a>
                            </li>
                            <li class="nav-item">
                                <div class="btn dropdown">
                                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Gerente General
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </nav>
                </div>
                <div class="col-lg-6 col-xl-4">
                    <nav class="navbar navbar-expand-lg navegacion">
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav justify-content-between">
                                <?php
                                $titulos=["Inicio","Cursos","Progreso","Preguntas"];
                                $links=["index.php","cursos.php","progreso.php","preguntas.php"];
                                for($x = 0; $x <4; $x++){
                                    echo '<li class="nav-item ';
                                    if($indice==$x){
                                        echo 'active';
                                    }
                                    echo '"><a class="nav-link" href="'.$links[$x].'"><span>'.$titulos[$x].'</span>';
                                    if($indice==$x){
                                        echo '<span class="sr-only">(current)</span>';
                                    }
                                    echo '</a></li>';
                                }
                                ?>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fas fa-search"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#"><i class="fas fa-bars"></i></a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            
            
        </div>
    </header>