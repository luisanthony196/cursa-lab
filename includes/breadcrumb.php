<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <?php
        for ($x = 0; $x <count($ruta); $x++) {
            if($x<count($ruta)-1){
                echo '<li class="breadcrumb-item"><a href="'.$links[$ruta[$x]].'">'.$titulos[$ruta[$x]].'</a></li>';
            }else{
                echo '<li class="breadcrumb-item active" aria-current="page">'.$titulos[$ruta[$x]].'</li>';
            }
        }
        ?>
    </ol>
</nav>